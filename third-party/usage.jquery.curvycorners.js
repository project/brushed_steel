  $(document).ready(function(){
    settings = {
      tl: { radius: 5 },
      tr: { radius: 5 },
      bl: false,
      br: false,
      antiAlias: true,		// Turning off anti-aliasing will reduce the smoothness of the corners
      autoPad: false,		// By default and DIVs that make use of the curvyCorners can't have padding applied
      validTags: ["li"]
    }

    $("div#primary ul.links-menu li").curvy(settings);

    settings = {
      tl: { radius: 7 },
      tr: { radius: 7 },
      bl: { radius: 7 },
      br: { radius: 7 },
      antiAlias: true,		// Turning off anti-aliasing will reduce the smoothness of the corners
      autoPad: false,		// By default and DIVs that make use of the curvyCorners can't have padding applied
      validTags: ["div"]
    }

    $("div.sidebar div.block").curvy(settings);
    $("div#mission").curvy(settings);
  });
