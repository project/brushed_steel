<div class="<?php print $node_classes ?> clear-both" id="node-<?php print $node->nid; ?>">
  <?php if ($page == 0): ?>
    <h2 class="title">
      <a href="<?php print $node_url ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>

  <?php if ($picture) print $picture; ?>

  <div class="meta clear-block clear">
	<?php if ($submitted): ?> <span class="submitted"><?php print t('Posted ') . format_date($node->created, 'custom', "F jS, Y") . t(' by ') . theme('username', $node); ?></span> <?php endif; ?>
	<?php if (count($taxonomy)): ?> <span class="taxonomy"><?php print t(' in ') . $terms ?></span> <?php endif; ?>
  </div>

  <div class="content clear-block clear">
    <?php print $content; ?>
  </div>

  <?php if ($links): ?> <?php print $links; ?> <?php endif; ?>

  <hr id="node-separator" />

</div>
