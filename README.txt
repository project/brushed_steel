README

Theme Name: brushed_steel
Theme Version: 1.0
Theme Author: http://neemtree.com.au
Drupal version compatibility: 5.x

Install instructions (retired - updated project to include /third-party/* under GPL - 08/08/2007):

 - decompress/unzip to your Drupal site's themes folder, and add the required JQuery port of Curvy Corners js file.

 -- download jquery.curvycorners.js from http://chingrimaachh.com.au/files/jquery-curvycorners/jquery.curvycorners.js 
 -- and save to themes/brushed_steel/third-party/
 -- result should be themes/brushed_steel/third-party/jquery.curvycorners.js and themes/brushed_steel/third-party/usage.jquery.curvycorners.js 
 
 FYI: Curvy Corners (http://www.curvycorners.net) uses LGPL licensing, which is incompatible with Drupal's GPL policy, and therefore can not be bundled with this install. Please complete the following steps to finish your theme installation.
